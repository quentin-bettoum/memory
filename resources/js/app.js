require("./bootstrap");
import Vue from "vue";

import Memorygrid from "./views/memorygrid.vue";

new Vue({
  el: "#memory-game",
  components: { Memorygrid }
});
