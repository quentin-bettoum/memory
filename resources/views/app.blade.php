<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Memory</title>
        <link rel="icon" href="images/tux.svg" sizes="any" type="image/svg+xml">
        <link href="/css/app.css" rel="stylesheet">
    </head>
    <body class="antialiased">
        <h1>Memory game</h1>
        <form method="POST" action="/" class="number-of-pairs-form">
            @csrf
            <label for="email">Number of pairs</label>
            <input name="pairsNumber" type="number" value={{$numberOfPairs}} min="4" max="{{$numberOfImages}}">
            <input type="submit" value="Submit">
        </form>
        <div class="leaderboard">
            <h2>Leaderboard</h2>
            @if(count($bestScores) > 0)
            <ol>
                @foreach($bestScores as $key => $score)
                <li>{{ ++$key }}# {{ $score['timeToFinish'] }} seconds</li>
                @endforeach
            </ol>
            @else
            <p>No score at the moment.</p>
            @endif
        </div>
        <memorygrid id="memory-game" v-bind:game-array-count="@json($gameArrayCount)" v-bind:countdown-timer="@json($countdownTimer)"></memorygrid>
        <footer>
            <div>
                <p>Source code available on <a href="https://gitlab.com/quentin-bettoum/memory" rel="noopener" target="_blank">Gitlab ⇗</a></p>
                <p>Icons made by <a href="https://www.freepik.com" rel="noopener" target="_blank" title="Freepik">Freepik ⇗</a> from <a href="https://www.flaticon.com/" rel="noopener" target="_blank" title="Flaticon">Flaticon ⇗</a></p>
            </div>
        </footer>
        <script src="{{ mix('js/app.js') }}"></script>
    </body>
</html>