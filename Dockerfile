#+++++++++++++++++++++++++++++++++++++++
# Dockerfile for webdevops/php-apache:8.0
#    -- automatically generated  --
#+++++++++++++++++++++++++++++++++++++++

FROM webdevops/php-apache:8.0

RUN set -x \
    && apt-install default-mysql-client
