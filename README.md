# Memory game

This is a memory game made in PHP (with Laravel) and JavaScript (with VueJS)

## Installation instructions

Create you **.env** file and customize it as you want

```bash
cp .env.example .env
```

Install PHP dependencies with **Composer**

```bash
composer install
```

Use [Laravel Sail](https://laravel.com/docs/8.x/sail) for your development environment.
You can configure a Bash alias

```bash
alias sail='bash vendor/bin/sail'
```

Start the docker container

```bash
#With Alias
sail up -d

#Without Alias
./vendor/bin/sail up -d
```

Install JS dependencies with **NPM**

```bash
sail npm install
```

Run migrations (wait for the MySQL container to start)

```bash
sail artisan migrate
```

Access the app on your browser at **http://localhost**

Stop the docker container

```bash
sail down
```

To compile JS and SCSS files with NPM

```bash
#Live refresh for development
sail npm run watch

#Compile for prod
sail npm run prod
```
