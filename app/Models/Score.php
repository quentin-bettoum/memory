<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Score extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'timeToFinish'
    ];

    /**
     * Get the best scores
     * @return Array
     */
    public function bestScores()
    {
        $scores = self::select('timeToFinish')
        ->orderBy('timeToFinish')
        ->limit(10)
        ->get();

        return $scores->map(function ($score) {
            return collect($score->toArray())
                ->only('timeToFinish')
                ->all();
        })->all();
    }
}
