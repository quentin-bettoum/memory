<?php
namespace App\Http\Controllers;

use App\Models\Score;
use Illuminate\Http\Request;

class GameController extends Controller
{
    public $minNumberOfPairsAllowed = 4;
    public $numberOfPairs = 12;
    public $countdownTimer = 60;
    public $gameArray = [];
    public $alreadyFoundCards = [];
    public $imageNames = ['bat','beaver','bee','beetle','boar','buffalo','bullfinch','butterfly','camel','cat','chameleon','chicken','clown fish','cow','crab','crocodile','deer','dog','elephant','flamingo','fox','frog','giraffe','gorilla','hedgehog','hippo','horse','ladybug','lama','lion','mouse','owl','panda','parrot','penguin','pig','platypus','rabbit','rhino','shark','sheep','sloth','snake','spider','squid','stingray','turtle','walrus','whale','zebra'];

    /**
     * Instantiate a new controller instance.
     */
    public function __construct()
    {
        if(is_null(session('gameArray'))) {
            $this->generateGameArray();
            $this->shuffleImageNames();
        } else {
            $this->gameArray = session('gameArray');
        }
        $this->alreadyFoundCards = session('alreadyFoundCards') ?? [];
    }
    
    /**
     * initGame
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function initGame(Request $request)
    {
        if(!is_null($request->pairsNumber) && $request->pairsNumber >= $this->minNumberOfPairsAllowed && $request->pairsNumber <= count($this->imageNames))
        $this->numberOfPairs = $request->pairsNumber ?? $this->numberOfPairs;
        $this->generateGameArray();
        $this->shuffleImageNames();
        $score = new Score();
        $bestScores = $score->bestScores();
        return view(
            'app', 
            [
                'gameArrayCount' => count($this->gameArray),
                'countdownTimer' => $this->countdownTimer,
                'bestScores' => $bestScores,
                'numberOfImages' => count($this->imageNames),
                'minNumberOfPairsAllowed' => $this->minNumberOfPairsAllowed,
                'numberOfPairs' => $this->numberOfPairs
            ]
        );
    }

    /**
     * Reset the game data
     */
    public function flushGameSession() {
        session()->forget([
            'gameArray',
            'pairsFound',
            'imageNames',
            'alreadyFoundCards',
            'pairsFound',
            'firstFlippedCardPosition'
        ]);
    }

    /**
     * Generate a random game array and save it in Session
     */
    public function generateGameArray() 
    {
        $this->flushGameSession();
        $cardsIdsArray = range(0, $this->numberOfPairs - 1);
        // Merge two arrays of ints with an id for each pair in the grid
        $this->gameArray = array_merge($cardsIdsArray, $cardsIdsArray);
        shuffle($this->gameArray);
        session(['gameArray' => $this->gameArray]);
        session(['pairsFound' => 0]);
    }

    /**
     * Shuffle imageNames array to have random images each game
     */
    public function shuffleImageNames() {
        shuffle($this->imageNames);
        session(['imageNames' => $this->imageNames]);
    }

    /**
     * Process the card flipped by the user
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function flipCard(Request $request) 
    {
        if($request->input('flippedCardPosition') !== NULL && is_int($request->input('flippedCardPosition'))) {
            $flippedCardPosition = $request->input('flippedCardPosition');
            $pairFound = NULL;
            $allPairsFound = false;
            $firstFlippedCardPosition = session('firstFlippedCardPosition');
            
            if(in_array($flippedCardPosition, $this->alreadyFoundCards)) {
                return response('This pair has already been found', 409);
            } elseif($firstFlippedCardPosition === $flippedCardPosition) {
                return response('This card is already flipped', 409);
            }
            
            // If it's the first flipped card
            if(is_null($firstFlippedCardPosition)) {
                session(['firstFlippedCardPosition' => $flippedCardPosition]);
            } else { // Else, it is the second card
                session()->forget('firstFlippedCardPosition');
                $pairFound = $this->isPairWithfirstFlippedCardPosition($flippedCardPosition, $firstFlippedCardPosition);
                // If a pair is found, and all cards have been found
                if($pairFound && count($this->alreadyFoundCards) === count($this->gameArray)) {
                    $allPairsFound = true;
                    $this->flushGameSession();
                }
            }

            return response([
                'cardName' => $this->imageNames[$this->gameArray[$flippedCardPosition]], // integer
                'pairFound' => $pairFound, // NULL | boolean
                'allPairsFound' => $allPairsFound // boolean
            ], 200);
        }
        return response('Bad request', 400);
    }

    /**
     * Checks if the second flipped card is pair with the first flipped card
     * @param int $flippedCardPosition 
     * @param int $firstFlippedCardPosition 
     * @return bool
     */
    public function isPairWithfirstFlippedCardPosition(int $flippedCardPosition, int $firstFlippedCardPosition) : bool
    {
        $pairFound = $this->gameArray[$firstFlippedCardPosition] === $this->gameArray[$flippedCardPosition];
        if($pairFound) {
            session()->increment('pairsFound');
            // session(['pairsFound' => session('pairsFound') + 1]);
            array_push($this->alreadyFoundCards, $firstFlippedCardPosition, $flippedCardPosition);
            session(['alreadyFoundCards' => $this->alreadyFoundCards]);
        }
        return $pairFound;
    }

    /**
     * Register a new score in database
     * @param Request $requets
     */
    public function registerScore(Request $request) {
        if($request->input('timeToFinish') !== NULL && is_numeric($request->input('timeToFinish'))) {
            $score = new Score;
            $score->timeToFinish = $request->input('timeToFinish');
            $score->save();
            return response(status: 200);
        }
        return response('Bad request', 400);
    }
}

